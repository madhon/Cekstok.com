<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct() {
		parent::__construct();
		//$this->load->model(array('Mod_Login'));
		if (!isset($this->session->userdata['id_user'])) {
			redirect(base_url("Login"));
		}
	}

	public function index() {
		$this->load->view('admin');
	}
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('Login'));
	}
}
?>
