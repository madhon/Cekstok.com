<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
	public function index(){
		$data =["category"]=$this->category_m->get_all();
		$this->load->view("category",$data);
	}
	public function search(){
		$keyword = $this->input->post("keyword");
		$data["category"] = $this->category_m->get_category_keyword($keyword);
		$this->load->view("search",$data);
	}
}
?>