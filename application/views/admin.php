
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login ku</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap/bootstrap.min.css')?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/AdminLTE.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/font-awesome.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/ionicons.min.css')?>">

</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
   Berhasil Login
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Login Sukses</p>

    <form action="<?php echo base_url('login/proses'); ?>" method="post">
      <?php
        if (validation_errors() || $this->session->flashdata('result_login')) {
      ?>
            <div class="alert alert-danger animated fadeInDown" role="alert">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Peringatan!</strong>
                <?php echo validation_errors(); ?>
                <?php echo $this->session->flashdata('result_login'); ?>
            </div>
      <?php } ?>
      <h2><?php echo $this->session->flashdata('success'); ?></h2>
      <h2>Hai, <?php echo $this->session->userdata('nama_user'); ?></h2>
     
      <div class="row">
        <div class="col-xs-4 pull-right">
          <a href="<?php echo base_url('Datauser/logout'); ?>">Logout</a>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <center><strong>Copyright &copy; 2018 <a href="#">PKL SMK PGRI WLINGI</a>.</strong> 
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url('assets/js/plugins/jQuery/jquery-2.2.3.min.js')?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.min.js')?>"></script>
</body>
</html>
