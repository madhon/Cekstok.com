<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login ku</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap/bootstrap.min.css')?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/AdminLTE.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/fonts/font-awesome.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/ionicons.min.css')?>">

</head>
<body class="hold-transition login-page" style="background: <?php echo base_url('assets/img/bg.gif') ?> no-repeat center center fixed;-webkit-background-size: cover;-moz-background-size: cover;background-size: cover;-o-background-size: cover;">
<div class="login-box">
  <div class="login-logo">
   DATA USERKU
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">DATA USER</p>

    <form action="<?php echo base_url('login/proses'); ?>" method="post">
  
      <div class="form-group has-feedback">
        <input type="text" name="username" class="form-control" placeholder="Username" autofocus="true">
        <span class="fa fa-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password">
        <span class="fa fa-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4 pull-right">
          <button type="submit" id="btn_login" class="btn btn-default pull-right">User</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <center><strong>Copyright &copy; 2018 <a href="#">PKL SMK PGRI WLINGI</a>.</strong> 
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url('assets/js/plugins/jQuery/jquery-2.2.3.min.js')?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.min.js')?>"></script>
</body>
</html>
